export ROOT_PATH = $(PWD)

all: dependency

dependency:
	make -C dependency
	find out/lib -name '*\.la' | xargs rm

clean:
	make clean -C dependency

.PHONY: dependency
