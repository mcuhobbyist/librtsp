TARGET = libuv

ARGS = --prefix=$(PREFIX) --disable-static

include $(ROOT_PATH)/config.mk

ifeq ($(USING_EXTERNAL_LIBUV),y)
	# ignore to build this library
else
	TARGETS += $(TARGET)
endif

$(TARGET):
	@cd library/$(TARGET) && \
	if [ ! -f configure ]; then \
		echo "Generate configure ..."; \
		sh autogen.sh; \
	fi; \
	if [ ! -f Makefile ]; then \
		echo "Generate Makefile ..."; \
		./configure $(ARGS); \
	fi; \
	make -j$(CPUS) && \
	make install && \
	cd - || cd -

$(TARGET)_clean:
	cd library/$(TARGET) && \
	if [ -f Makefile ]; then \
		make distclean; \
	fi; \
	cd -
